import React, {useState} from "react";
import ExpenseFilter from "./components/ExpenseFilter/ExpenseFilter";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";
import mockExpenses from "./expense-data";
function App() {
 const  [expenses, setExpensesHandler] = useState(mockExpenses);

  const addExpenseHandler = (value) => {
    setExpensesHandler((prevState)=>{
      return [...prevState, value]
    })
  }


  return (
    <div>
      <NewExpense onSaveExpenseData={addExpenseHandler} />
      <Expenses  expenses={expenses} />
    </div>
  );
}

export default App;
