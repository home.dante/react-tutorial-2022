  const mockExpenses = [
    {
      id: "1",
      title: "Billy Herrington",
      amount: "300$",
      date: new Date(2020, 7, 13),
    },
    {
      id: "2",
      title: "Van Darkholme",
      amount: "300$",
      date: new Date(2021, 7, 13),
    },
    {
      id: "3",
      title: "Steve Rambo",
      amount: "300$",
      date: new Date(2022, 7, 13),
    },
    {
      id: "4",
      title: "Billy Herrington",
      amount: "300$",
      date: new Date(2019, 7, 13),
    },
    {
      id: "5",
      title: "Billy Herrington",
      amount: "300$",
      date: new Date(2020, 7, 13),
    },
    {
      id: "6",
      title: "Billy Herrington",
      amount: "300$",
      date: new Date(2020, 7, 13),
    },
  ];
  export default mockExpenses;