import React, { useState } from "react";
import validateNewExpense from "../../shared/validators";
import "./ExpenseForm.css";
const ExpenseForm = (props) => {
  const defaultValue = new Date().toISOString().split('T')[0]
  const [newExpenseObject, setNewExpenseObject] = useState(
    {
      title: '',
      amount: 0,
      date: defaultValue
    }
  );
  const submitFormHandler = (event) => {
    event.preventDefault();
    if (!validateNewExpense(newExpenseObject)) {
      return;
    }
    const expenseData = {
      ...newExpenseObject,
      date: new Date(newExpenseObject.date)
    }
    // console.log(expenseData);
    props.onSaveExpenseData(expenseData)
    setNewExpenseObject({
      title: '',
      amount: 0,
      date: defaultValue
    })

  };

  // const setAmount = (event) => { setNewExpenseObject((prevState) => { return { ...prevState, amount: event.target.value } }); };
  // const setDate = (event) => { setNewExpenseObject((prevState) => { return { ...prevState, date: event.target.value } }); };
  // const setTitle = (event) => { setNewExpenseObject((prevState) => { return { ...prevState, title: event.target.value } }); };

  const onChangeHandler = (event) => {
    setNewExpenseObject( prevState => { return { ...prevState, [event.target.name]: event.target.value } });
  }
  return (
    <form>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input minLength={3} value={newExpenseObject.title} autoFocus={true} type="text" name="title" onChange={onChangeHandler}></input>
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input value={newExpenseObject.date} type="date" onChange={onChangeHandler} name="date" min="2019-01-01" max="2022-12-31"></input>
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input value={newExpenseObject.amount} type="number" onChange={onChangeHandler} name="amount" min="0.01" step="0.01"></input>
        </div>
      </div>
      <div className="new-expense__actions">
        <button type="submit" onClick={submitFormHandler} >Add Expense</button>
      </div>
    </form>
  );
};

export default ExpenseForm;
