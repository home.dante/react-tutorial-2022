import React, { useState } from "react";
import ExpenseFilter from "../ExpenseFilter/ExpenseFilter";
import Card from "../UI/Card";
import ExpenseItem from "./ExpenseItem";
import "./Expenses.css";
import ExpensesList from "./ExpensesList";
function Expenses(props) {
  const expenses = props.expenses;
  const [filteredYear, setFilteredYear] = useState("2020");
  const expenseFilter = (expense, year) => {
    return (
      new Date(expense.date).getFullYear() == year || year == "Select Year"
    );
  };
  // const [filteredExpenses, filterExpenses] = useState(
  //   expenses.filter((expense) => expenseFilter(expense, filteredYear))
  // );
  const getFilteredExpenses = () => {
    return expenses.filter((expense) => expenseFilter(expense, filteredYear));
  };
  const onApplyFilter = (year) => {
    setFilteredYear(() => year);
    // filterExpenses(() => {
    //   return getFilteredExpenses();
    // });
  };
  return (
    <div>
      <Card className="expenses">
        <ExpenseFilter
          selected={filteredYear}
          onApplyFilter={onApplyFilter}
        ></ExpenseFilter>
        <ExpensesList expenses={getFilteredExpenses()}></ExpensesList>
      </Card>
    </div>
  );
}
export default Expenses;
