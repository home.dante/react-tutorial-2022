import React from "react";
import Card from "../UI/Card";
import "./ExpenseFilter.css";
const ExpenseFilter = (props) => {
  const applyFilter = (event) => {
    if (event.target.value === null) return;
    props.onApplyFilter(event.target.value);
  };
  return (
    <div className="expenses-filter">
      <div className="expenses-filter__control">
        <label>Filter by year</label>
        <select value={props.selected} onChange={applyFilter}>
          <option value={null}>Select Year</option>
          <option value="2018">2018</option>
          <option value="2019">2019</option>
          <option value="2020">2020</option>
          <option value="2021">2021</option>
          <option value="2022">2022</option>
        </select>
      </div>
    </div>
  );
};

export default ExpenseFilter;
